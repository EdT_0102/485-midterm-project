﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Pause : MonoBehaviour
{
  public static bool GameIsPaused = false;
  public GameObject pauseMenuUI;

  public void QuitGame(){
    Debug.Log("Quit!");
    Application.Quit();
  }

  public void Menu(){
    Time.timeScale = 1.0f;
    SceneManager.LoadScene("MainMenu");

  }

  void Update() {
    if(Input.GetKeyDown(KeyCode.Escape))
    {
      if(GameIsPaused)
      {
        Resume();
      }
      else{
        PauseGame();
      }

    }
  }

  public void Resume ()
  {
    pauseMenuUI.SetActive(false);
    Time.timeScale = 1.0f; //resets game back to normal state
    GameIsPaused = false;
  }

  void PauseGame ()
  {
    pauseMenuUI.SetActive(true);
    Time.timeScale = 0.0f; //freezes time for the game
    GameIsPaused = true;
  }


}
