﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class playerhp : MonoBehaviour
{
    public int Playerhealth=10;
    public bool isTouching = false;
    public float maxDistance = 5;
    bool waiting = false;
    public GameObject can;

    public bool win = false;

    void Start()
    {
        Playerhealth = 10;

    }
    void Update ()
    {
        if(Playerhealth<0)
        {
            StartCoroutine(end());
        }
        if(win == true)
        {


        }
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log(other);
        // other object is close
        if (Vector3.Distance(other.transform.position, this.transform.position) < maxDistance)
        {
            if (other.gameObject.tag == "enemy" && waiting ==false)
            {

                StartCoroutine(wait());
            }
            isTouching = true; // they are touching AND close
        }
        else
        {
            isTouching = false;
        }
    }

    IEnumerator wait()
    {
        waiting = true;
        yield return new WaitForSecondsRealtime(2);
        Playerhealth--;
        waiting = false;
    }
    IEnumerator end()
    {
        can.SetActive(true);
        yield return new WaitForSecondsRealtime(5);
        Restart();

    }
    IEnumerator won()
    {

        yield return new WaitForSecondsRealtime(5);
        winner();

    }
    void Restart()
    {

        SceneManager.LoadScene(2);
    }
    void winner()
    {

        SceneManager.LoadScene(2);
    }
}
